import tornado.ioloop
import tornado.web
import json
import os

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("home.html")

class GetInfoHandler(tornado.web.RequestHandler):
    def get(self):
        answer = {}
        answer["status"] = "Ok"
        answer["response"] = ["Grafos", "Graficos de Pie"]
        answer["response"].append("Grafico de barras")
        answer["response"].append("Grafico de lineas")
        answer["response"].append("Treemaps")

        answer_json = json.dumps(answer)
        self.write(answer_json)

    def initialize(self):
        pass

settings = {"template_path" : os.path.dirname(__file__),
            "static_path" : os.path.join(os.path.dirname(__file__),"static"),
            "debug" : True
            } 

if __name__ == "__main__":
    print('loading...')

    application = tornado.web.Application([
        (r"/", MainHandler),
        (r"/get_data", GetInfoHandler)
    ], **settings)
    application.listen(3000)
    print("ready")
    tornado.ioloop.IOLoop.current().start()