import tornado.ioloop
import tornado.web
import os
import pandas as pd
import json
import numpy as np

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("home.html")

settings = {"template_path" : os.path.dirname(__file__),
            "static_path" : os.path.join(os.path.dirname(__file__),"static"),
            "debug" : True
            } 

if __name__ == "__main__":
    path = os.path.join(os.path.dirname(__file__), "static/data/park-movement-Fri.csv")
    print('loading...')
    df = pd.read_csv(path)
    df["time"] = pd.to_datetime(df.Timestamp, format="%Y-%m-%d %H:%M:%S")

    application = tornado.web.Application([
        (r"/", MainHandler)
    ], **settings)
    application.listen(3100)
    print("ready")
    tornado.ioloop.IOLoop.current().start()